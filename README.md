# README #

Some my custom library fir DipTrace.
Include STM8 & STM32 chips, some other elements.

## STM8 ##
 * STM8S003F3P6 TSSOP-20
 * STM8Sx03Kx LQFP-32
 * STM8S103K3T6 LQFP-32
 * STM8S105 LQFP-44
 * STM8S105 LQFP-48
 * STM8L051F3P6 TSSOP-20
 * STM8L151Cx LQFP-48
 * STM8L151Kx LQFP-32
 * STM8L152Cx LQFP-48
 * STM8L152Kx LQFP-32

## STM32F0 ##
 * STM32F030F4P6 TSSOP-20
 * STM32F031F4P6 TSSOP-20
 * STM32F030K6T6 LQFP-32
 * STM32F030C6T6 LQFP-48
 * STM32F051C6T6 LQFP-48
 * STM32F030C8T6 LQFP-48
 * STM32F072C8T6 LQFP-48
 * STM32F030CCT6 LQFP-48
 * STM32F030R8T6 LQFP-64
 * STM32F030RCT6 LQFP-64
 * STM32F071RBT6 LQFP-64

## STM32F1 ##
 * STM32F100C4T6 LQFP-48
 * STM32F100C8T6 LQFP-48
 * STM32F103C8T6 LQFP-48
 * STM32F103VET6 LQFP-100

## STM32G0 ##
 * STM32G030FxP6 TSSOP-20
 * STM32G030KxT6 LQFP-32
 * STM32G031JxM6 SOIC-8
 * STM32G031FxP6 TSSOP-20
 * STM32G031GxU6 UFQFPN-28
 * STM32G031KxT6 LQFP-32

## STM32L1 ##
 * STM32L100R8T6 LQFP-64
 * STM32L152CBT6 LQFP-48

## Nixie Tube Lmap ##
 * INS-1
 * IN-3
 * IN-4
 * IN-8-2
 * IN-12
 * IN-15A
 * IN-15B
 * IN-14
 * IN-19A
 * IN-19B
 * IN-19V
 * IN-18

## VFD Tube Lamp ##
 * IV-9
 * IV-17

## Displays ##
 * JLX12864G-086
 * Nokia 3310
 * Nokia 1202
 * Nokia 1216
 * VQC 10
 * Led Matrix 1.9 8x8
 * TFT LCD 128x160 1.8"

## 7-segment indicators ##
 * 1x 1.5"
 * 1x 1.2"
 * 1x 0.8"
 * 2x 0.36"
 * 3x 0.36"
 * 4x 0.56"
 * 4x 1.2" - E41201

## 14-segment indicators ##
 * 2x 0.54" - PDC54/PDA54

## Modules from ALi ##
 * Wemos D1 Mini
 * DS3231
 * GY-302 (BH1750)
 * BME-280
 * BME-680
 * GY-49
 * AHT10
 * AGCK (MP2315)
 * MHT-ET LIVE (MAX30102)
 * SX1276
 * MP3-TF-16P (DFPlayer Mini)
 * Arduino ProMini 
 * BluePill
 * ZMPT101B
